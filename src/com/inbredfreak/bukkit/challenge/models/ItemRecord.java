package com.inbredfreak.bukkit.challenge.models;
/**
 * 
 * @author InbredFreak
 * @version 0.1
 *
 */
public class ItemRecord {

	// Properties
	private String item = "AIR";
	private int id = 0;
	private int dmg = 0;
	private int qty = 0;
	

	/**
	 * 
	 * @return
	 */
	public String getItem() 
	{
		return item;
	}

	/**
	 * 
	 * @param item
	 */
	public void setItem(String item) 
	{
		this.item = item;
	}
	
	/**
	 * 
	 * @return (int) item id
	 */
	public int getId() 
	{
		return id;
	}
	
	/**
	 * 
	 * @param ir_id (int) item id
	 */
	public void setId(int id) 
	{
		this.id = id;
	}
	
	/**
	 * 
	 * @return(int) item damage
	 */
	public int getDmg() 
	{
		return dmg;
	}
	
	/**
	 * 
	 * @param ir_dmg (int) item damage
	 */
	public void setDmg(int dmg) 
	{
		this.dmg = dmg;
	}
	
	/**
	 * 
	 * @return (int) item quantity
	 */
	public int getQty() 
	{
		return qty;
	}
	
	/**
	 * 
	 * @param ir_qty (int) item quantity
	 */
	public void setQty(int qty) 
	{
		this.qty = qty;
	}
	
	/**
	 * 
	 * @param id (int) item id
	 * @param dmg (int) item damage
	 * @param qty (int) item quantity
	 */
	public void setir(String item, int id, int dmg, int qty)
	{
		this.setItem(item);
		this.setId(id);
		this.setDmg(dmg);
		this.setQty(qty);
	}
}
