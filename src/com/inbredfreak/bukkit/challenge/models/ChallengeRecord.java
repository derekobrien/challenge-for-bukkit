package com.inbredfreak.bukkit.challenge.models;

public class ChallengeRecord 
{

	// Properties
	private String challenge;
	private String achieved;
	private ItemRecord slot0;
	private ItemRecord slot1;
	private ItemRecord slot2;
	private ItemRecord slot3;
	private ItemRecord slot4;
	private ItemRecord slot5;
	private ItemRecord slot6;
	private ItemRecord slot7;
	private ItemRecord slot8;
	
	/**
	 * 
	 * @return
	 */
	public String getChallenge() 
	{
		return challenge;
	}
	/**
	 * 
	 * @param challenge
	 */
	public void setChallenge(String challenge) 
	{
		this.challenge = challenge;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getAchieved() 
	{
		return achieved;
	}
	/**
	 * 
	 * @param achieved
	 */
	public void setAchieved(String achieved) 
	{
		this.achieved = achieved;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot0() 
	{
		return slot0;
	}
	/**
	 * 
	 * @param slot0
	 */
	public void setSlot0(ItemRecord slot0) 
	{
		this.slot0 = slot0;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot1() 
	{
		return slot1;
	}
	/**
	 * 
	 * @param slot1
	 */
	public void setSlot1(ItemRecord slot1) 
	{
		this.slot1 = slot1;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot2() 
	{
		return slot2;
	}
	/**
	 * 
	 * @param slot2
	 */
	public void setSlot2(ItemRecord slot2) 
	{
		this.slot2 = slot2;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot3() 
	{
		return slot3;
	}
	/**
	 * 
	 * @param slot3
	 */
	public void setSlot3(ItemRecord slot3) 
	{
		this.slot3 = slot3;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot4() 
	{
		return slot4;
	}
	/**
	 * 
	 * @param slot4
	 */
	public void setSlot4(ItemRecord slot4) 
	{
		this.slot4 = slot4;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot5() 
	{
		return slot5;
	}
	/**
	 * 
	 * @param slot5
	 */
	public void setSlot5(ItemRecord slot5) 
	{
		this.slot5 = slot5;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot6() 
	{
		return slot6;
	}
	/**
	 * 
	 * @param slot6
	 */
	public void setSlot6(ItemRecord slot6) 
	{
		this.slot6 = slot6;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot7() 
	{
		return slot7;
	}
	/**
	 * 
	 * @param slot7
	 */
	public void setSlot7(ItemRecord slot7) 
	{
		this.slot7 = slot7;
	}
	
	/**
	 * 
	 * @return
	 */
	public ItemRecord getSlot8() 
	{
		return slot8;
	}
	/**
	 * 
	 * @param slot8
	 */
	public void setSlot8(ItemRecord slot8) 
	{
		this.slot8 = slot8;
	}
	/**
	 * 
	 * @param challenge
	 * @param achieved
	 * @param s0item
	 * @param s0id
	 * @param s0dmg
	 * @param s0qty
	 * @param s1item
	 * @param s1id
	 * @param s1dmg
	 * @param s1qty
	 * @param s2item
	 * @param s2id
	 * @param s2dmg
	 * @param s2qty
	 * @param s3item
	 * @param s3id
	 * @param s3dmg
	 * @param s3qty
	 * @param s4item
	 * @param s4id
	 * @param s4dmg
	 * @param s4qty
	 * @param s5item
	 * @param s5id
	 * @param s5dmg
	 * @param s5qty
	 * @param s6item
	 * @param s6id
	 * @param s6dmg
	 * @param s6qty
	 * @param s7item
	 * @param s7id
	 * @param s7dmg
	 * @param s7qty
	 * @param s8item
	 * @param s8id
	 * @param s8dmg
	 * @param s8qty
	 */
	public void setChallengeRecord (
			String challenge,
			String achieved,
			String s0item, int s0id, int s0dmg, int s0qty,
			String s1item, int s1id, int s1dmg, int s1qty, 
			String s2item, int s2id, int s2dmg, int s2qty, 
			String s3item, int s3id, int s3dmg, int s3qty, 
			String s4item, int s4id, int s4dmg, int s4qty, 
			String s5item, int s5id, int s5dmg, int s5qty, 
			String s6item, int s6id, int s6dmg, int s6qty, 
			String s7item, int s7id, int s7dmg, int s7qty,  
			String s8item, int s8id, int s8dmg, int s8qty
	)
	{
		this.challenge = challenge;
		this.achieved = achieved;
		
		this.slot0 = new ItemRecord();
		this.slot0.setItem(s0item);
		this.slot0.setId(s0id);
		this.slot0.setDmg(s0dmg);
		this.slot0.setQty(s0qty);
		
		this.slot1 = new ItemRecord();
		this.slot1.setItem(s1item);
		this.slot1.setId(s1id);
		this.slot1.setDmg(s1dmg);
		this.slot1.setQty(s1qty);
		
		this.slot2 = new ItemRecord();
		this.slot2.setItem(s2item);
		this.slot2.setId(s2id);
		this.slot2.setDmg(s2dmg);
		this.slot2.setQty(s2qty);
		
		this.slot3 = new ItemRecord();
		this.slot3.setItem(s3item);
		this.slot3.setId(s3id);
		this.slot3.setDmg(s3dmg);
		this.slot3.setQty(s3qty);
		
		this.slot4 = new ItemRecord();
		this.slot4.setItem(s4item);
		this.slot4.setId(s4id);
		this.slot4.setDmg(s4dmg);
		this.slot4.setQty(s4qty);
		
		this.slot5 = new ItemRecord();
		this.slot5.setItem(s5item);
		this.slot5.setId(s5id);
		this.slot5.setDmg(s5dmg);
		this.slot5.setQty(s5qty);
		
		this.slot6 = new ItemRecord();
		this.slot6.setItem(s6item);
		this.slot6.setId(s6id);
		this.slot6.setDmg(s6dmg);
		this.slot6.setQty(s6qty);
		
		this.slot7 = new ItemRecord();
		this.slot7.setItem(s7item);
		this.slot7.setId(s7id);
		this.slot7.setDmg(s7dmg);
		this.slot7.setQty(s7qty);
		
		this.slot8 = new ItemRecord();
		this.slot8.setItem(s8item);
		this.slot8.setId(s8id);
		this.slot8.setDmg(s8dmg);
		this.slot8.setQty(s8qty);
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public ItemRecord getSlot(int id)
	{
		ItemRecord item;
		switch(id)
		{
			case 0:
				item = this.slot0;
				break;
			case 1:
				item =  this.slot1;
				break;
			case 2:
				item =  this.slot2;
				break;
			case 3:
				item =  this.slot3;
				break;
			case 4:
				item =  this.slot4;
				break;
			case 5:
				item =  this.slot5;
				break;
			case 6:
				item =  this.slot6;
				break;
			case 7:
				item =  this.slot7;
				break;
			case 8:
				item =  this.slot8;
				break;
			default:
				item = new ItemRecord();
		}
		return item;
	}
}
