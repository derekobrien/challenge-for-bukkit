package com.inbredfreak.bukkit.challenge.models;

public class Achievements {


    private int achievements_id;
    
    private String achievements_ign;

    private int achievements_date;
    
    private String achievements_challenge;

    /**
     * 
     * @return (int) Achievement id
     */
    public int getAchievements_id() 
    {
		return achievements_id;
	}
    
	public void setAchievements_id(int achievements_id) 
	{
		this.achievements_id = achievements_id;
	}
    
    /**
     * 
     * @return (String) Achievement players in game name
     */
	public String getAchievements_ign() 
	{
		return achievements_ign;
	}

	/**
	 * 
	 * @param achievements_ign (String) Achievement players in game name
	 */
	public void setAchievements_ign(String achievements_ign) 
	{
		this.achievements_ign = achievements_ign;
	}

	/**
	 * 
	 * @return (Date) Achievement players date/time of achievement
	 */
	public int getAchievements_date() 
	{ 
		return achievements_date; 
	}
	
	/**
	 * 
	 * @param achievements_date (Date) Achievement players date/time of achievement
	 */
	public void setAchievements_date(int achievements_date) 
	{ 
		this.achievements_date = achievements_date; 
	}
	  
	/**
	 * 
	 * @return (String) Challenge id
	 */
	public String getAchievements_challenge() 
	{
		return achievements_challenge;
	}

	/**
	 * 
	 * @param achievements_challenge (String) Challenge id
	 */
	public void setAchievements_challenge(String achievements_challenge) 
	{
		this.achievements_challenge = achievements_challenge;
	}
}
