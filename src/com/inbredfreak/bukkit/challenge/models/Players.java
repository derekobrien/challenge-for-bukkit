package com.inbredfreak.bukkit.challenge.models;

public class Players 
{

    private String player_ign;
    
    private boolean player_promoted;
    
    private int player_cooldown;

	/**
	 * 
	 * @return (int) Player in game name
	 */
	public String getPlayer_ign() 
	{
		return player_ign;
	}

	/**
	 * 
	 * @param player_ign (int) Player in game name
	 */
	public void setPlayer_ign(String player_ign) 
	{
		this.player_ign = player_ign;
	}
	
	/**
	 * 
	 * @return (boolean) Player promotion status
	 */
	public boolean getPlayer_promoted ()
	{
		return player_promoted;
	}
	
	/**
	 * 
	 * @param set player_promoted (boolean) Player promotion status
	 */
	public void setPlayer_promoted ( boolean player_promoted )
	{
		this.player_promoted = player_promoted;
	}
	
	/**
	 * 
	 * @return (int) Player cooldown in seconds
	 */
	public int getPlayer_cooldown ()
	{
		return player_cooldown;
	}
	
	/**
	 * 
	 * @param player_ign (int) Player cooldown in seconds
	 */
	public void setPlayer_cooldown ( int player_cooldown )
	{
		this.player_cooldown = player_cooldown;
	}

}
