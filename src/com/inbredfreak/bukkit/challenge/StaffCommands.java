package com.inbredfreak.bukkit.challenge;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.entity.Player;

import com.inbredfreak.bukkit.challenge.models.Achievements;


public class StaffCommands 
{
	private Player plyer;
	private Challenge plugin;
	private String userParams;

	/**
	 * 
	 * @param plyer
	 * @param plugin
	 */
	public StaffCommands (Player plyer, Challenge plugin)
	{
		this.plyer = plyer;
		this.plugin = plugin;
        this.userParams = "/ca reload | spam | <player> <get|remove|give> [<1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + ">]";
	}
	
	/**
	 * 
	 * @param commandLabel
	 * @param args
	 */
	public void check(String commandLabel, String[] args)
	{
		if(commandLabel.equalsIgnoreCase("ca"))
		{
			List <Player> matchedPlayers;
			switch(args.length) 
			{
			case 0:  // ./ca
               plyer.sendMessage( "This command requires parameters " + this.userParams );
               break;
				
			case 1: // ./ca <playername>
				if(args[0].equalsIgnoreCase("spam"))
				{
					// check if already listed
					List< Player > ps = this.plugin.getActiveStaff ();
					boolean activePlayer = false;
					
					if(ps.size () > 0) // fix for null list
					{
						for(Player p:ps)
						{
							if(p == this.plyer)
							{
								// remove
								activePlayer = true;
								p.sendMessage ( "Challenge Player Spam Disabled!!" );
								ps.remove ( p );
								break;
							}
						}
					}
					
					if(activePlayer == false)
					{
						// add
						ps.add ( this.plyer );
						this.plyer.sendMessage ( "Challenge Player Spam Enabled!" );
					}
					
				}
				else if(args[0].equalsIgnoreCase("reload"))
				{
					this.plugin.reloadConfig ();
					this.plugin.saveConfig ();
					plyer.sendMessage( "Plugin config reloaded!" );
				}
				else
				{
					matchedPlayers = this.plugin.getServer().matchPlayer(args[0].toLowerCase());
					
					if(matchedPlayers.size() > 0)
					{
						Player p = (Player) matchedPlayers.get(0);
	
						//++++++++++++++++++++++
						// Check if Player has Promotion
						//++++++++++++++++++++++
						boolean promoted = false;
						try
						{
							PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_promoted FROM players WHERE player_ign = '" + p.getPlayerListName () + "'" );
							ResultSet checkResult = playerStatement.executeQuery( );
							if (checkResult.last () != false)
							{
								checkResult.beforeFirst ();
								while (checkResult.next())
							    {
									promoted = Boolean.parseBoolean(checkResult.getString ( "player_promoted" ));
							    }
							}
							checkResult.close ();
							this.plugin.closeDatabase ();
						}
						catch ( SQLException e )
						{
							e.printStackTrace();
						}
						
						if(promoted == true)
						{
							plyer.sendMessage( p.getPlayerListName () + " Challenge Status");
							plyer.sendMessage( "=================================");
				    		plyer.sendMessage( "According to server records this player has completed");
					    	plyer.sendMessage( "all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + " challenge tasks and should already be promoted.");
						} 
						else 
						{
							//++++++++++++++++++++++
							// Get All Players Achievements
							//++++++++++++++++++++++
					    	List <Achievements> aList = new ArrayList <Achievements>();
					    	try
							{
								PreparedStatement achievementsStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_id as achievementID, UNIX_TIMESTAMP(player_achievements_date) as achievementDate, player_achievements_challenge as achievementChallenge FROM player_achievements WHERE player_achievements_ign = '" + p.getPlayerListName ()+ "' ORDER BY player_achievements_challenge ASC" );
								ResultSet achievementsResult = achievementsStatement.executeQuery( );
								if (achievementsResult.last ()  != false)
								{
									achievementsResult.beforeFirst ();
									while (achievementsResult.next())
								    {
										Achievements achievement = new Achievements();
										achievement.setAchievements_id ( achievementsResult.getInt ( "achievementID" ) );
										achievement.setAchievements_ign ( p.getPlayerListName () );
										achievement.setAchievements_date ( achievementsResult.getInt ( "achievementDate" ) );
										achievement.setAchievements_challenge ( achievementsResult.getString ( "achievementChallenge" ) );
										aList.add ( achievement );
								    }
								}
								this.plugin.closeDatabase ();
							}
							catch ( SQLException e )
							{
								e.printStackTrace();
							}
					    	plyer.sendMessage( p.getName() + " Challenge Status");
							plyer.sendMessage( "=================================");
							
							String status = "";
	
							boolean aCheck = false;
							Date aDate;
							for (int i = 0; i < this.plugin.getConfig().getInt( "challenge.challenges.tasks" ); i++)
							{
								int j = i+1;
								for(int k = 0; k < aList.size(); k++)
								{
									if ( Pattern
												.compile (
														Pattern.quote ( aList.get(k).getAchievements_challenge() ),
														Pattern.CASE_INSENSITIVE )
												.matcher ( Integer.toString(j) )
												.find () )
									{
										aDate = new Date(Long.parseLong("" + aList.get(k).getAchievements_date()) * 1000);
										status =  "Completed! " + aDate;
										aCheck = true;
									}
								}
								if(aCheck == false)
								{
									status =  "Incomplete!";
								}
								plyer.sendMessage( "Challenge Task " + j + ": " + status);
								aCheck = false;
							}
						}
					} else {
						this.plyer.sendMessage( "No players match " +  args[0]);
					}
				}
				break;
				
			case 2: // ./ca <playername> <get|remove>
				plyer.sendMessage( "Missing third paramenter" + this.userParams);
				break;
				
			case 3: // ./ca <playername> <get|remove> <1-3>
				int v = Integer.valueOf(args[2]);
				
				if(args[1].equalsIgnoreCase("get"))
				{
					if(v > 0 && v <= this.plugin.getConfig().getInt( "challenge.challenges.tasks" ))
					{
							matchedPlayers = this.plugin.getServer().matchPlayer(args[0].toLowerCase());
							
							if(matchedPlayers.size() > 0)
							{
								Player p = (Player) matchedPlayers.get(0);
								String  challengeID = Integer.toString(v);
			
								//++++++++++++++++++++++
								// Check if Player has Promotion
								//++++++++++++++++++++++
								boolean promoted = false;
								try
								{
									PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_promoted FROM players WHERE player_ign = '" + p.getPlayerListName () + "'");
									ResultSet checkResult = playerStatement.executeQuery( );
									if (checkResult.last () != false)
									{
										checkResult.beforeFirst ();
										while (checkResult.next())
									    {
											promoted = Boolean.parseBoolean(checkResult.getString ( "player_promoted" ));
									    }
									}
									checkResult.close ();
									this.plugin.closeDatabase ();
								}
								catch ( SQLException e )
								{
									e.printStackTrace();
								}
								
								if(promoted == true)
								{
									plyer.sendMessage( p.getPlayerListName () + " Challenge Status" );
									plyer.sendMessage( "=================================");
						    		plyer.sendMessage( "According to server records this player has completed");
							    	plyer.sendMessage( "all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + " challenge tasks and should already be promoted.");
								} else {
									//++++++++++++++++++++++
									// Get All Players Achievements
									//++++++++++++++++++++++
							    	List <Achievements> aList = new ArrayList <Achievements>();
							    	try
									{
										PreparedStatement achievementsStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_id as achievementID, UNIX_TIMESTAMP(player_achievements_date) as achievementDate, player_achievements_challenge as achievementChallenge FROM player_achievements WHERE player_achievements_ign = '" + p.getPlayerListName ()+ "'  AND player_achievements_challenge = '" + challengeID + "' ORDER BY player_achievements_challenge ASC" );
										ResultSet achievementsResult = achievementsStatement.executeQuery( );
										if (achievementsResult.last ()  != false)
										{
											achievementsResult.beforeFirst ();
											while (achievementsResult.next())
										    {
												Achievements achievement = new Achievements();
												achievement.setAchievements_id ( achievementsResult.getInt ( "achievementID" ) );
												achievement.setAchievements_ign ( p.getPlayerListName () );
												achievement.setAchievements_date ( achievementsResult.getInt ( "achievementDate" ) );
												achievement.setAchievements_challenge ( achievementsResult.getString ( "achievementChallenge" ) );
												aList.add ( achievement );
										    }
										}
										this.plugin.closeDatabase ();
									}
									catch ( SQLException e )
									{
										e.printStackTrace();
									}
			
									Date aDate;
									if(aList != null)
									{
										if(aList.size() == 1)
										{
											aDate = new Date(Long.parseLong("" + aList.get(0).getAchievements_date()) * 1000);
											plyer.sendMessage( p.getName() + " Challenge Status");
											plyer.sendMessage( "=================================");
											plyer.sendMessage( "Challenge Task " + args[2] + ": Completed! " + aDate);
										} else if (aList.size() == 0) {
											plyer.sendMessage( p.getName() + " Challenge Status");
											plyer.sendMessage( "=================================");
											plyer.sendMessage( "Challenge Task " + args[2] + ": Incomplete!");
											
										} else {
											plyer.sendMessage( "Player " + args[0] + " has no records for that challenge!");
										}
									}
								}
							} else {
								this.plyer.sendMessage( "No players match " +  args[0]);
							}
					}
					else
					{
							plyer.sendMessage( "Unrecognised third parameter" + this.userParams);
					}
				} else if (args[1].equalsIgnoreCase("remove")) 
				{
					if(v > 0 && v <= this.plugin.getConfig().getInt( "challenge.challenges.tasks" ))
					{
							matchedPlayers = this.plugin.getServer().matchPlayer(args[0].toLowerCase());
							
							if(matchedPlayers.size() > 0)
							{
								Player p = (Player) matchedPlayers.get(0);
								String  challengeID = Integer.toString(v);
			
								//++++++++++++++++++++++
								// Check if Player has Promotion
								//++++++++++++++++++++++
								boolean promoted = false;
								try
								{
									PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_promoted FROM players WHERE player_ign = '" + p.getPlayerListName () + "'");
									ResultSet checkResult = playerStatement.executeQuery( );
									if (checkResult.last () != false)
									{
										checkResult.beforeFirst ();
										while (checkResult.next())
									    {
											promoted = Boolean.parseBoolean(checkResult.getString ( "player_promoted" ));
									    }
									}
									checkResult.close ();
									this.plugin.closeDatabase ();
								}
								catch ( SQLException e )
								{
									e.printStackTrace();
								}
								
								if(promoted == true)
								{
									plyer.sendMessage( p.getPlayerListName () + " Challenge Status");
									plyer.sendMessage( "=================================");
						    		plyer.sendMessage( "According to server records this player has completed");
							    	plyer.sendMessage( "all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + " challenge tasks and should already be promoted.");
								} else {
									//++++++++++++++++++++++
									// Delete Players Achievements
									//++++++++++++++++++++++
							    	try
									{
							    		Statement st = this.plugin.getConn().createStatement();
										String sql = "DELETE FROM player_achievements WHERE player_achievements_ign = '" + p.getPlayerListName ()+ "'  AND player_achievements_challenge = '" + challengeID + "'";
										int delete = st.executeUpdate(sql);
										if(delete == 1){
											plyer.sendMessage( p.getName() + " Challenge Status");
											plyer.sendMessage( "=================================");
											plyer.sendMessage( " Task " + args[2] + ": Deleted!");
										}
										else{
											plyer.sendMessage( p.getName() + " Challenge Status");
											plyer.sendMessage( "=================================");
											plyer.sendMessage( "Challenge Task " + args[2] + ": Nothing to delete!");
										}
										this.plugin.closeDatabase ();
									}
									catch ( SQLException e )
									{
										e.printStackTrace();
									}
								}
							} else {
								this.plyer.sendMessage( "Player " + args[0] + " not online!");
							}
					}
					else
					{
							plyer.sendMessage( "Unrecognised third parameter" + this.userParams);
					}
				}
				else if (args[1].equalsIgnoreCase("give")) 
				{
					if (plyer.hasPermission ( "challenge.staff") == true)
					{
						if(v > 0 && v <= this.plugin.getConfig().getInt( "challenge.challenges.tasks" ))
						{
								matchedPlayers = this.plugin.getServer().matchPlayer(args[0].toLowerCase());
								
								if(matchedPlayers.size() > 0)
								{
									Player p = (Player) matchedPlayers.get(0);
									String  challengeID = Integer.toString(v);
				
									//++++++++++++++++++++++
									// Check if Player has Promotion
									//++++++++++++++++++++++
									boolean promoted = false;
									try
									{
										PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_promoted FROM players WHERE player_ign = '" + p.getPlayerListName () + "'");
										ResultSet checkResult = playerStatement.executeQuery( );
										if (checkResult.last () != false)
										{
											checkResult.beforeFirst ();
											while (checkResult.next())
										    {
												promoted = Boolean.parseBoolean(checkResult.getString ( "player_promoted" ));
										    }
										}
										checkResult.close ();
										this.plugin.closeDatabase ();
									}
									catch ( SQLException e )
									{
										e.printStackTrace();
									}
									
									if(promoted == true)
									{
										plyer.sendMessage( p.getPlayerListName () + " Challenge Status");
										plyer.sendMessage( "=================================");
							    		plyer.sendMessage( "According to server records this player has completed");
								    	plyer.sendMessage( "all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + " challenge tasks and should already be promoted.");
									} else {
										// check if player already has that challenge
										String completedChallenge = "null";
										try
										{
											PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_challenge FROM player_achievements WHERE player_achievements_ign = '" + p.getPlayerListName () + "' AND player_achievements_challenge = '" + challengeID + "'");
											ResultSet checkResult = playerStatement.executeQuery( );
											if (checkResult.last () != false)
											{
												checkResult.beforeFirst ();
												while (checkResult.next())
											    {
													completedChallenge = checkResult.getString ( "player_achievements_challenge" );
											    }
											}
											checkResult.close ();
											this.plugin.closeDatabase ();
										}
										catch ( SQLException e )
										{
											e.printStackTrace();
										}
										
										if ( Pattern
												.compile (
														Pattern.quote ( completedChallenge ),
														Pattern.CASE_INSENSITIVE )
												.matcher ( challengeID )
												.find () )
										{
											plyer.sendMessage( p.getPlayerListName () + " Challenge Status");
											plyer.sendMessage( "=================================");
								    		plyer.sendMessage( "According to server records this player has already");
									    	plyer.sendMessage( "completed the challenge you requested ID: " + challengeID  + ".");
										}
										else
										{
											// if false add challenge
											//++++++++++++++++++++
											// GENERATE COOLDOWN
											//++++++++++++++++++++
								    		int delay = (30 + (int)(Math.random() * ((180 - 30) + 1))) * 60;
											
								    		// get current time as long (unixtime)
								    		long nowInt = System.currentTimeMillis()/1000;
								    		
								    		// update achievements records (db and local)
								    		try
								    		{
								    			Statement newAchievementStatement = this.plugin.getConn().createStatement ();
									    		String newAchievement = "INSERT INTO player_achievements " +
									    														"(player_achievements_ign, player_achievements_date, player_achievements_challenge) " +
									    														"VALUES " +
									    														"('" + p.getPlayerListName() + "', FROM_UNIXTIME(" + nowInt + "),  '" + challengeID  + "')";
									    		int newAchievementResult = newAchievementStatement.executeUpdate ( newAchievement );
												
									    		if(newAchievementResult > 0)
												{
													try
													{
														Statement newCooldownStatement = this.plugin.getConn().createStatement ();
														String newCooldown = "UPDATE players SET player_cooldown = FROM_UNIXTIME(" + (nowInt+delay) + ") WHERE player_ign = '" + p.getPlayerListName () + "'";
											    		int newCooldownResult = newCooldownStatement.executeUpdate ( newCooldown );
														if(newCooldownResult > 0)
														{
															// and throw success
															plyer.sendMessage( "=================================");
													    	plyer.sendMessage( p.getPlayerListName () + " Challenge Status");
															plyer.sendMessage( "=================================");
												    		plyer.sendMessage( "This player has been given the challenge ID: " + challengeID  + ".");
												    		plyer.sendMessage( "Player has been notified of this intervention.. ");
													    	plyer.sendMessage( "Please ensure you do not abuse this command.");
															plyer.sendMessage( "=================================");
						
															p.sendMessage( "=================================");
													    	p.sendMessage( plyer.getDisplayName () + " has granted you challenge " + challengeID + ".");
													    	p.sendMessage( "Thank them for being so nice! " );
															p.sendMessage( "=================================");
														}
													}
													catch ( SQLException e )
													{
														e.printStackTrace();
													}
												} else {
													plyer.sendMessage("Possible fail to add player to db.");
												}
								    		}
											catch ( SQLException e )
											{
												e.printStackTrace();
											}
										}
									}
								} else {
									this.plyer.sendMessage( "No players match " +  args[0]);
								}
						}
						else
						{
								plyer.sendMessage( "Unrecognised third parameter" + this.userParams);
						}
					}
					else
					{
						this.plyer.sendMessage( "You do not have the required permission for this command.");
					}
				}
				else
				{
					plyer.sendMessage( "Unrecognised second parameter" + this.userParams);
				}
				break;
				
			default:
				plyer.sendMessage( "Sorry, to many parameters entered! - " + this.userParams);
				break;
			}
		}
	}
}
