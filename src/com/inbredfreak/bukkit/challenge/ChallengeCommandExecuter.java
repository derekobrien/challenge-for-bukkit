package com.inbredfreak.bukkit.challenge;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ChallengeCommandExecuter implements CommandExecutor {

    private Challenge plugin;
	private Player plyer;
 
	/**
	 * 
	 * @param plugin (Challenge) Plugin Document Class
	 * @param log (Logger) To announce messages to console
	 */
    public ChallengeCommandExecuter(Challenge plugin) {
    	
        this.plugin = plugin;
    }
    
    /**
     * 
     * @param sender (CommandSender) Player object
     * @param cmd (Command)
     * @param commandLabel (String) Command sent by player
     * @param args (String[]) Array of additional commands
     */
    public boolean onCommand(CommandSender sender, Command cmd, String commandLabel, String[] args)
	{
		this.plyer = (Player) sender;
		PlayerCommands acornCommands = new PlayerCommands(this.plyer, this.plugin);
		StaffCommands staffCommands = new StaffCommands(this.plyer, this.plugin);
			
			if (commandLabel.equalsIgnoreCase("challenge"))
			{
				if(this.plugin.getConfig ().getBoolean ( "challenge.commands.long.challenge" ))
				{
					acornCommands.check(commandLabel, args);
				}
			}
			else if (commandLabel.equalsIgnoreCase("c"))
			{
				if(this.plugin.getConfig ().getBoolean ( "challenge.commands.short.c" ))
				{
					acornCommands.check(commandLabel, args);
				}
			}
			else if (commandLabel.equalsIgnoreCase("ca"))
			{
				staffCommands.check(commandLabel, args);
			}
			else 
			{
				this.plyer.sendMessage("Command not recognised!");
			}
		return true;
	}
}