package com.inbredfreak.bukkit.challenge;

/**
 * 
 * @author InbredFreak
 * @version 0.1
 *
 */

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import com.inbredfreak.bukkit.challenge.models.ChallengeRecord;

public class Challenge extends JavaPlugin
{
	public Logger log;
	private List<ChallengeRecord> challengeData;

	private Connection conn;
	
	private List< Player > activeStaff = new ArrayList< Player > ();
    
	@Override
	public void onDisable()
	{ 
		this.log = this.getLogger();
		PluginDescriptionFile pdfFile = this.getDescription();

		this.closeDatabase ();
		
		this.log.info(pdfFile.getName() + " Version: " + pdfFile.getVersion() + " Has Been Disabled");
	}
	
	
	@Override
	public void onEnable ()
	{
		this.log = this.getLogger();
		PluginDescriptionFile pdfFile = this.getDescription();
		
		this.loadConfiguration ();
		this.loadChallengeData();
		this.setupDatabase ();

		
		this.getCommand ( "c" ).setExecutor ( new ChallengeCommandExecuter ( this ) );
		this.getCommand ( "challenge" ).setExecutor ( new ChallengeCommandExecuter ( this ) );
		this.getCommand ( "ca" ).setExecutor ( new ChallengeCommandExecuter ( this ) );
		
		this.log.info( pdfFile.getName() + " Version: " + pdfFile.getVersion() + " Has Been Enabled" );
	}
	
	public void loadConfiguration()
    {
    	String path = "challenge.";

    	this.getConfig().addDefault(path+"server.name", "Server");
    	this.getConfig().addDefault(path+"web.interface", "http://inbredfreak.com/bukkit/challenge/");
    	
    	this.getConfig().addDefault(path+"debug.enable", false);
    	this.getConfig().addDefault(path+"commands.short.c",  false);
    	this.getConfig().addDefault(path+"commands.long.challenge",  true);
    	
    	this.getConfig().addDefault(path+"database.host",  "localhost");
    	this.getConfig().addDefault(path+"database.port",  "3306");
    	this.getConfig().addDefault(path+"database.db",  "challenge");
    	this.getConfig().addDefault(path+"database.user",  "root");
    	this.getConfig().addDefault(path+"database.pass",  "root");

    	this.getConfig().addDefault(path+"promote.permissions.permissionsEx", false);
    	
    	this.getConfig().addDefault(path+"promote.teleport.enable", false);
    	this.getConfig().addDefault(path+"promote.teleport.fromworld", "world");
    	this.getConfig().addDefault(path+"promote.teleport.toworld", "world2");
    	this.getConfig().addDefault(path+"promote.teleport.worldname", "Main");
    	this.getConfig().addDefault(path+"promote.teleport.x", 206);
    	this.getConfig().addDefault(path+"promote.teleport.y", 67);
    	this.getConfig().addDefault(path+"promote.teleport.z", -371);

    	this.getConfig().addDefault(path+"promote.group.enable", false);
    	String [] worlds =
    		{ 
    		"null",
    		"world"
    		};
    	this.getConfig().addDefault(path+"promote.group.worlds", worlds);
    	String [] cmds =
    		{ 
    		"manudel",
    		"manuadd"
    		};
    	this.getConfig().addDefault(path+"promote.group.commands", cmds);
    	String [] names =
    		{ 
    		"Guest",
    		"Default"
    		};
    	this.getConfig().addDefault(path+"promote.group.names", names);
    	
    	this.getConfig().addDefault(path+"promote.hero.channel.enable", false);
    	this.getConfig().addDefault(path+"promote.hero.channel.from", "g");
    	this.getConfig().addDefault(path+"promote.hero.channel.to", "pro");

    	this.getConfig().addDefault(path+"cooldown.enable", true);

    	String [] challengeIntroNew =
    		{ 
    		"As a new citizen you will need to completing a", 
    		"challenge before you can be promoted to", 
    		"full server citizenship where you will have", 
    		"access to our main world!", 
    		"Use '/challenge [1|2|3]' to get more instructions."
    		};
    	String [] challengeIntroExisting =
    		{ 
    		"You should already know what you should be doing!", 
            "So get on with it.."
    		};
    	String [] challengeIntro1 =
    		{ 
    		"There are three challenge tasks to complete.", 
			"Type '/challenge task <1-3>'", 
			"to view requirements for each task.", 
			"This command will also notify you if you have", 
			"already completed the specific task.", 
			"Use '/challenge [1|2|3]' to get more instructions."
    		};
    	String [] challengeIntro2 =
    		{ 
    		"Once you have collected all the items for a task,", 
			"start with an empty  hotbar (all 9 slots empty)", 
			"Then place the 'correct items and quantities'", 
			"into the 'correct slots' then type '/challenge check'", 
			"This will notify whether you have been successful or not.", 
			"To check the status of all your challenge tasks", 
			"type '/challenge status'.", 
	        "Use '/challenge [1|2|3]' to get more instructions.", 
    		};
    	String [] challengeIntro3 =
    		{ 
    		"When you have completed all three tasks", 
			"type '/challenge promote'", 
			"If promotion is enabled you will be promoted", 
			"immediately or notified otherwise.", 
			"Be prepared for teleportation, rank change", 
			"and herochat channel change if applicable.", 
    		"Use '/challenge [1|2|3]' to get more instructions."
    		};
    	String [] taskInfo =
    		{ 
    		"You need to complete three item based challenge tasks.",
    		"To get challenge task info type '/challenge task <1-3>'."
    		};
    	String [] commandsInfo =
    		{ 
    		"./challenge", 
        	"./challenge  [1|2|3]", 
        	"./challenge task", 
        	"./challenge task <1-3>", 
        	"./challenge check", 
        	"./challenge status", 
        	"./challenge promote"
    		};
    	

    	this.getConfig().addDefault(path+"messages.introduction.user.new", challengeIntroNew);
    	this.getConfig().addDefault(path+"messages.introduction.user.existing", challengeIntroExisting);
    	
    	this.getConfig().addDefault(path+"messages.instructions.page.one", challengeIntro1);
    	this.getConfig().addDefault(path+"messages.instructions.page.two", challengeIntro2);
    	this.getConfig().addDefault(path+"messages.instructions.page.three", challengeIntro3);

    	this.getConfig().addDefault(path+"messages.task.information", taskInfo);

    	this.getConfig().addDefault(path+"messages.commands.information", commandsInfo);
    	

    	this.getConfig().addDefault(path+"challenges.tasks", 3);
    	
    	this.getConfig().addDefault(path+"challenges.1.task", "Get 'Wood'");
    	this.getConfig().addDefault(path+"challenges.1.message", "Achievement 'Wood'");
    	this.getConfig().addDefault(path+"challenges.1.delay.min", 60);
    	this.getConfig().addDefault(path+"challenges.1.delay.max", 180);
    	this.getConfig().addDefault(path+"challenges.1.0.item", "Wood");
    	this.getConfig().addDefault(path+"challenges.1.0.id", 5);
    	this.getConfig().addDefault(path+"challenges.1.0.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.0.qty", 1);

    	this.getConfig().addDefault(path+"challenges.1.1.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.1.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.1.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.1.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.2.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.2.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.2.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.2.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.3.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.3.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.3.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.3.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.4.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.4.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.4.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.4.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.5.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.5.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.5.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.5.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.6.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.6.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.6.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.6.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.7.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.7.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.7.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.7.qty", 0);

    	this.getConfig().addDefault(path+"challenges.1.8.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.1.8.id", 0);
    	this.getConfig().addDefault(path+"challenges.1.8.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.1.8.qty", 0);
    	

    	this.getConfig().addDefault(path+"challenges.2.task", "Get 'Cake is not a Lie'");
    	this.getConfig().addDefault(path+"challenges.2.message", "Achievement 'Cake is not a Lie'");
    	this.getConfig().addDefault(path+"challenges.2.delay.min", 60);
    	this.getConfig().addDefault(path+"challenges.2.delay.max", 180);
    	this.getConfig().addDefault(path+"challenges.2.0.item", "Cake");
    	this.getConfig().addDefault(path+"challenges.2.0.id", 354);
    	this.getConfig().addDefault(path+"challenges.2.0.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.0.qty", 1);

    	this.getConfig().addDefault(path+"challenges.2.1.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.1.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.1.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.1.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.2.item", "Empty");  
    	this.getConfig().addDefault(path+"challenges.2.2.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.2.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.2.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.3.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.3.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.3.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.3.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.4.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.4.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.4.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.4.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.5.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.5.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.5.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.5.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.6.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.6.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.6.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.6.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.7.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.7.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.7.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.7.qty", 0);

    	this.getConfig().addDefault(path+"challenges.2.8.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.2.8.id", 0);
    	this.getConfig().addDefault(path+"challenges.2.8.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.2.8.qty", 0);
    	

    	this.getConfig().addDefault(path+"challenges.3.task", "Get 'Suits You Sir!'");
    	this.getConfig().addDefault(path+"challenges.3.message", "Achievement 'Suits You Sir!'");
    	this.getConfig().addDefault(path+"challenges.3.delay.min", 60);
    	this.getConfig().addDefault(path+"challenges.3.delay.max", 180);
    	this.getConfig().addDefault(path+"challenges.3.0.item", "Iron Helmet");
    	this.getConfig().addDefault(path+"challenges.3.0.id", 306);
    	this.getConfig().addDefault(path+"challenges.3.0.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.0.qty", 1);

    	this.getConfig().addDefault(path+"challenges.3.1.item", "Iron Chestplate");
    	this.getConfig().addDefault(path+"challenges.3.1.id", 307);
    	this.getConfig().addDefault(path+"challenges.3.1.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.1.qty", 1);

    	this.getConfig().addDefault(path+"challenges.3.2.item", "Iron Leggings");
    	this.getConfig().addDefault(path+"challenges.3.2.id", 308);
    	this.getConfig().addDefault(path+"challenges.3.2.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.2.qty", 1);

    	this.getConfig().addDefault(path+"challenges.3.3.item", "Iron Boots");
    	this.getConfig().addDefault(path+"challenges.3.3.id", 309);
    	this.getConfig().addDefault(path+"challenges.3.3.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.3.qty", 1);

    	this.getConfig().addDefault(path+"challenges.3.4.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.3.4.id", 0);
    	this.getConfig().addDefault(path+"challenges.3.4.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.4.qty", 0);

    	this.getConfig().addDefault(path+"challenges.3.5.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.3.5.id", 0);
    	this.getConfig().addDefault(path+"challenges.3.5.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.5.qty", 0);

    	this.getConfig().addDefault(path+"challenges.3.6.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.3.6.id", 0);
    	this.getConfig().addDefault(path+"challenges.3.6.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.6.qty", 0);

    	this.getConfig().addDefault(path+"challenges.3.7.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.3.7.id", 0);
    	this.getConfig().addDefault(path+"challenges.3.7.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.7.qty", 0);

    	this.getConfig().addDefault(path+"challenges.3.8.item", "Empty");
    	this.getConfig().addDefault(path+"challenges.3.8.id", 0);
    	this.getConfig().addDefault(path+"challenges.3.8.dmg", 0);
    	this.getConfig().addDefault(path+"challenges.3.8.qty", 0);
    	
        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
        
    }
	
    private boolean setupDatabase () 
    {
    	if(this.openDatabase ())
    	{
	    	try
	    	{
	    		if(this.getConn() != null)
	    		{
	    		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
	    			{
	    		    	this.log.info ( "Connected to database!");
	    			}
	    			// Create tables if they do not already exist
	    			Statement statement = this.conn.createStatement ();
	    			String createTable = "";
	    			
	    			createTable= "CREATE TABLE IF NOT EXISTS players (player_ign varchar(16) NOT NULL UNIQUE KEY, player_promoted enum('true','false') NOT NULL default 'false', player_cooldown datetime NOT NULL )";
	    			statement.executeUpdate ( createTable );
	    			
	    			createTable= "CREATE TABLE IF NOT EXISTS player_achievements (player_achievements_id int(11) NOT NULL auto_increment PRIMARY KEY, player_achievements_ign varchar(16) NOT NULL, player_achievements_date datetime NOT NULL, player_achievements_challenge int(11) NOT NULL)";
	    			statement.executeUpdate ( createTable );
	    			
	    			this.closeDatabase ();
	    		}
	    		else
	    		{
	    		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
	    			{
	    		    	this.log.info ( "Unable to connect to database!");
	    			}
	    		}
	    	} 
	    	catch ( SQLException e )
			{
	    		this.log.info ( e.getMessage() );
	    		return false;
			}
    	}
    	else
    	{
		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
			{
		    	this.log.info ( "Unable to open database connection" );
			}
    		return false;
    	}
    	return true;
    }
    
    private boolean openDatabase ()
	{
    	try
    	{
		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
			{
		    	this.log.info ( "Opening database connection" );
			}
	    	this.setConn ( null );
	    	Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://" + this.getConfig ().getString ( "challenge.database.host" ) + ":" + this.getConfig ().getString ( "challenge.database.port" ) + "/" + this.getConfig ().getString ( "challenge.database.db" ) + "?autoReconnect=true";
			this.setConn ( DriverManager.getConnection ( url, this.getConfig ().getString ( "challenge.database.user" ), this.getConfig ().getString ( "challenge.database.pass" ) ) );
		}
    	catch (ClassNotFoundException e) 
    	{
    		this.log.info ( e.getMessage() );
    		return false;
    		
    	}
		catch ( SQLException e )
		{
    		this.log.info ( e.getMessage() );
    		return false;
		} 
    	return true;
	}


    public boolean closeDatabase() 
    {
    	try
    	{
    		if(this.getConn() != null)
    		{
    		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
    			{
    		    	this.log.info ( "Closing database connection" );
    			}
    			this.conn.close ();
    			this.conn = null;
    		}
    		else 
    		{
    		    if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
    			{
    		    	this.log.info ( "No database connection to close!" );
    			}
        		return false;
    		}
    	} 
    	catch (SQLException e)
    	{
    		this.log.info ( e.getMessage() );
    		return false;
    	}
    	return true;
    }

	public Connection getConn () 
	{
		if(this.conn == null)
		{
			this.openDatabase ();
		}
		else
		{
			try
			{
				if(this.conn.isClosed () == true)
				{
					if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
					{
				    	this.log.info ( "Connection closed, making a new DB connection" );
					}
					this.openDatabase ();
				} 
				else 
				{
					if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
					{
				    	this.log.info ( "Reusing existing database connection" );
					}
				}
			}
			catch ( SQLException e )
			{
				if(this.getConfig ().getBoolean ( "challenge.debug.enable" ))
				{
			    	this.log.info ( "Forcing new DB connection and trying reconnect." );
				}
				this.openDatabase();
			}
		}
		return this.conn;
	}

	public void setConn ( Connection conn )
	{
		this.conn = conn;
	}
	
	 /**
     * 
     */
    public void loadChallengeData()
    {
		this.setChallengeData(new ArrayList<ChallengeRecord>());
    	for (int i = 0; i < 9; i++)
    	{
    		int j = i+1;
	    	ChallengeRecord item =  new ChallengeRecord();
	    	item.setChallengeRecord(
	    			this.getConfig().getString("challenge.challenges." + j + ".task"), 
	    			this.getConfig().getString("challenge.challenges." + j + ".message"), 
	    			this.getConfig().getString("challenge.challenges." + j + ".0.item"), this.getConfig().getInt("challenge.challenges." + j + ".0.id"), this.getConfig().getInt("challenge.challenges." + j + ".0.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".0.qty"), 
	    			this.getConfig().getString("challenge.challenges." + j + ".1.item"), this.getConfig().getInt("challenge.challenges." + j + ".1.id"), this.getConfig().getInt("challenge.challenges." + j + ".1.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".1.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".2.item"), this.getConfig().getInt("challenge.challenges." + j + ".2.id"), this.getConfig().getInt("challenge.challenges." + j + ".2.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".2.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".3.item"), this.getConfig().getInt("challenge.challenges." + j + ".3.id"), this.getConfig().getInt("challenge.challenges." + j + ".3.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".3.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".4.item"), this.getConfig().getInt("challenge.challenges." + j + ".4.id"), this.getConfig().getInt("challenge.challenges." + j + ".4.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".4.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".5.item"), this.getConfig().getInt("challenge.challenges." + j + ".5.id"), this.getConfig().getInt("challenge.challenges." + j + ".5.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".5.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".6.item"), this.getConfig().getInt("challenge.challenges." + j + ".6.id"), this.getConfig().getInt("challenge.challenges." + j + ".6.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".6.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".7.item"), this.getConfig().getInt("challenge.challenges." + j + ".7.id"), this.getConfig().getInt("challenge.challenges." + j + ".7.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".7.qty"),
	    			this.getConfig().getString("challenge.challenges." + j + ".8.item"), this.getConfig().getInt("challenge.challenges." + j + ".8.id"), this.getConfig().getInt("challenge.challenges." + j + ".8.dmg"), this.getConfig().getInt("challenge.challenges." + j + ".8.qty")
	    			);
	    	this.getChallengeData().add(item);
    	}
    }

    /**
     * 
     * @return
     */
	public List<ChallengeRecord> getChallengeData() {
		return challengeData;
	}

	/**
	 * 
	 * @param challengeData
	 */
	private void setChallengeData(List<ChallengeRecord> challengeData) {
		this.challengeData = challengeData;
	}


	public List< Player > getActiveStaff ()
	{
		return this.activeStaff;
	}


	public void setActiveStaff ( List< Player > activeStaff )
	{
		this.activeStaff = activeStaff;
	}
}

