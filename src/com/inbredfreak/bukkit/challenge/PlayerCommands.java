package com.inbredfreak.bukkit.challenge;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.inbredfreak.bukkit.challenge.models.Achievements;
import com.inbredfreak.bukkit.challenge.models.Players;
import com.inbredfreak.bukkit.challenge.models.ChallengeRecord;

public class PlayerCommands 
{
	private Challenge plugin;
	private Player plyer;	
	private boolean newPlyer;
	private String plyerIGN;
	private Players player;
	
	private int[] invItem = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	private int[] invQty = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	private short[] invDmg = {0, 0, 0, 0, 0, 0, 0, 0, 0};
	
	private String userParams;

	/**
	 * 
	 * @param plyer
	 * @param plugin
	 */
	public PlayerCommands (Player plyer, Challenge plugin)
	{
		this.plyer = plyer;
		this.plugin = plugin;
		this.plyerIGN = plyer.getPlayerListName();
		this.player = new Players();
        this.userParams = "/challenge [<1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + ">] | [task [<1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + ">] | check | status | promote | commands";
	}
	
	/**
	 * 
	 * @param commandLabel
	 * @param args
	 */
	public void check(String commandLabel, String[] args)
	{
		//check player is in the registered player table (players)
		try
		{
			// register player if not already registered
			PreparedStatement playerStatement = this.plugin.getConn().prepareStatement ("SELECT player_ign, player_promoted, UNIX_TIMESTAMP(player_cooldown) as unixTime FROM players WHERE player_ign = '" + this.plyerIGN + "'" );
			ResultSet checkResult = playerStatement.executeQuery( );
			if (checkResult.last ()  == false)
			{
				this.broadcastToPlayers ( "[CHALLENGE] " +  this.plyerIGN + " registered to play " + this.plugin.getConfig().getString( "challenge.server.name") + " Challenge" );
				
				/*
				long now = System.currentTimeMillis()/1000;
				int delay = (30 + (int)(Math.random() * ((180 - 30) + 1))) * 60;
				long cooldown = now+delay;
				*/
				long cooldown = System.currentTimeMillis()/1000;
				
				String regPlayer = "INSERT INTO players (player_ign, player_cooldown) VALUES ('" + this.plyerIGN + "', FROM_UNIXTIME(" + cooldown + "))" ;
				int regResult = playerStatement.executeUpdate ( regPlayer );
				if(regResult > 0)
				{
					this.player.setPlayer_ign ( this.plyerIGN );
					this.player.setPlayer_promoted ( false );
					this.player.setPlayer_cooldown ( (int) cooldown );
					this.plyer.sendMessage ( "You have been registered to play " + this.plugin.getConfig().getString( "challenge.server.name") + " Challenge" );
					this.newPlyer = true;
					
				} else {
					this.plyer.sendMessage ( "Failed to register, please notify an admin!" );
					this.newPlyer = false;
				}
			} else {
				checkResult.beforeFirst ();
				while (checkResult.next())
			    {
					this.player.setPlayer_ign ( checkResult.getString ( "player_ign" ));
					this.player.setPlayer_promoted ( Boolean.parseBoolean(checkResult.getString ( "player_promoted" )) );
					this.player.setPlayer_cooldown ( checkResult.getInt ( "unixTime" ));
			    }
				this.newPlyer = false;
			}
			checkResult.close ();
			this.plugin.closeDatabase ();
		}
		catch ( SQLException e )
		{
			// add report to config file?
			e.printStackTrace();
		}
		
		if(this.player.getPlayer_promoted () == false)
		{
	        // commands
	        if(commandLabel.equalsIgnoreCase("challenge") || commandLabel.equalsIgnoreCase("c"))
			{
				switch(args.length) 
				{
				case 0:  // ./challenge
					if(this.newPlyer == true)
	                {
	                	plyer.sendMessage("==================================");
	                	plyer.sendMessage("Welcome " + this.player.getPlayer_ign() + ",");
	                	plyer.sendMessage("==================================");
	                	
	                	List< String > intronew = this.plugin.getConfig ().getStringList ( "challenge.messages.introduction.user.new" );
						
	                	if(intronew.size() > 0)
	                	{
							for ( int i = 0; i < intronew.size (); i++ )
							{
								plyer.sendMessage( intronew.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
	    	            
		            } 
					else
					{
	                	plyer.sendMessage("==================================");
			            plyer.sendMessage("Hi "  + this.player.getPlayer_ign() + ",");
	                	plyer.sendMessage("==================================");
			            
	                	List< String > introexisting = this.plugin.getConfig ().getStringList ( "challenge.messages.introduction.user.existing" );
						
	                	if(introexisting.size() > 0)
	                	{
							for ( int i = 0; i < introexisting.size (); i++ )
							{
								plyer.sendMessage( introexisting.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
		            }
					break;
					
				case 1:
					if(args[0].equalsIgnoreCase("1")) // ./challenge 1
					{
	                	plyer.sendMessage("==================================");
						plyer.sendMessage("Page 1 of 3");
	                	plyer.sendMessage("==================================");
						
	                	List< String > introPageOne = this.plugin.getConfig ().getStringList ( "challenge.messages.instructions.page.one" );
	                	if(introPageOne.size() > 0)
	                	{
							for ( int i = 0; i < introPageOne.size (); i++ )
							{
								plyer.sendMessage( introPageOne.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("2")) // ./challenge 2
					{
	                	plyer.sendMessage("==================================");
						plyer.sendMessage("Page 2 of 3");
	                	plyer.sendMessage("==================================");
						
	                	List< String > introPageTwo = this.plugin.getConfig ().getStringList ( "challenge.messages.instructions.page.two" );
						
	                	if(introPageTwo.size() > 0)
	                	{
							for ( int i = 0; i < introPageTwo.size (); i++ )
							{
								plyer.sendMessage( introPageTwo.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("3")) // ./challenge 3
					{
	                	plyer.sendMessage("==================================");
						plyer.sendMessage("Page 3 of 3");
	                	plyer.sendMessage("==================================");
						
	                	List< String > introPageThree = this.plugin.getConfig ().getStringList ( "challenge.messages.instructions.page.three" );
						
	                	if(introPageThree.size() > 0)
	                	{
							for ( int i = 0; i < introPageThree.size (); i++ )
							{
								plyer.sendMessage( introPageThree.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("task")) // ./challenge task
					{
	                	plyer.sendMessage("==================================");
						plyer.sendMessage(this.plugin.getConfig ().getString ( "challenge.server.name" ) +  " Challenges");
	                	plyer.sendMessage("==================================");
						
	                	List< String > taskInfo = this.plugin.getConfig ().getStringList ( "challenge.messages.task.information" );
						
	                	if(taskInfo.size() > 0)
	                	{
							for ( int i = 0; i < taskInfo.size (); i++ )
							{
								plyer.sendMessage( taskInfo.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("commands")) // ./challenge commands
					{
	                	plyer.sendMessage("==================================");
						plyer.sendMessage(this.plugin.getConfig ().getString ( "challenge.server.name" ) +  " Challenges");
	                	plyer.sendMessage("==================================");
						
	                	List< String > commandsInfo = this.plugin.getConfig ().getStringList ( "challenge.messages.commands.information" );
						
	                	if(commandsInfo.size() > 0)
	                	{
							for ( int i = 0; i < commandsInfo.size (); i++ )
							{
								plyer.sendMessage( commandsInfo.get ( i ) );
							}
	                	}
	                	
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("check")) // ./challenge check
					{
						this.broadcastToPlayers ("[CHALLENGE] " +  this.plyerIGN + " task check!");
						
						// collate item in inventory
						for (int i = 0; i < 9; i++)
						{
							if(plyer.getInventory().getItem(i) != null)
							{
								this.invItem[i] = plyer.getInventory().getItem(i).getTypeId();
								this.invQty[i] = plyer.getInventory().getItem(i).getAmount();
								this.invDmg[i] = plyer.getInventory().getItem(i).getDurability();
							} 
							else 
							{
								this.invItem[i] = 0;
								this.invQty[i] = 0;
								this.invDmg[i] = 0;
							}
						}
						// initiate check
						this.checkChallenge();
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
						
					} 
					else if (args[0].equalsIgnoreCase("promote")) // ./challenge promote
					{
						for (int i = 0; i < 9; i++)
						{
							if(plyer.getInventory().getItem(i) != null)
							{
								this.invItem[i] = plyer.getInventory().getItem(i).getTypeId();
								this.invQty[i] = plyer.getInventory().getItem(i).getAmount();
								this.invDmg[i] = plyer.getInventory().getItem(i).getDurability();
							} 
							else 
							{
								this.invItem[i] = 0;
								this.invQty[i] = 0;
								this.invDmg[i] = 0;
							}
						}
						this.checkPromotion();

						
					} else if (args[0].equalsIgnoreCase("status")) // ./challenge status
					{
						
						// check if player has completed all challenges
						if(this.player.getPlayer_promoted ())
						{
		                	plyer.sendMessage("==================================");
							plyer.sendMessage(this.plugin.getConfig ().getString ( "challenge.server.name" ) +  " Challenge Status");
		                	plyer.sendMessage("==================================");
				    		plyer.sendMessage("According to our records you have completed");
					    	plyer.sendMessage("all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks" ) + " challenge tasks and you have already been promoted.");
					    	plyer.sendMessage("If this is wrong, please contact an admin.");
				            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
		                	plyer.sendMessage("==================================");
						}
						else
						{
							List <Achievements> aList = new ArrayList <Achievements>();
							String status = "";
							boolean aCheck = false;

		                	plyer.sendMessage("==================================");
							plyer.sendMessage(this.plugin.getConfig ().getString ( "challenge.server.name" ) +  " Challenge Status");
		                	plyer.sendMessage("==================================");
							
							try
							{
								PreparedStatement statusStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_id, UNIX_TIMESTAMP(player_achievements_date) as achievementDate, player_achievements_challenge FROM player_achievements WHERE player_achievements_ign = '" + this.player.getPlayer_ign() + "' ORDER BY player_achievements_challenge ASC" );
								ResultSet statusResult = statusStatement.executeQuery( );
								if (statusResult.last ()  != false)
								{
									statusResult.beforeFirst ();
									while (statusResult.next())
								    {
										Achievements achievement = new Achievements();
										achievement.setAchievements_id ( statusResult.getInt ( "player_achievements_id" ) );
										achievement.setAchievements_ign ( this.player.getPlayer_ign () );
										achievement.setAchievements_date ( statusResult.getInt ( "achievementDate" ) );
										achievement.setAchievements_challenge ( statusResult.getString ( "player_achievements_challenge" ) );
										aList.add ( achievement );
								    }
								}

								this.plugin.closeDatabase ();
							}
							catch ( SQLException e )
							{
								e.printStackTrace();
							}
	
							for (int i = 0; i < this.plugin.getConfig().getInt( "challenge.challenges.tasks" ); i++)
							{
								int j = i+1;
								for(int k = 0; k < aList.size(); k++)
								{
									
									if ( Pattern
												.compile (
														Pattern.quote ( aList.get(k).getAchievements_challenge() ),
														Pattern.CASE_INSENSITIVE )
												.matcher ( Integer.toString(j) )
												.find () )
									{
										aCheck = true;
									}
								}
								if(aCheck == true)
								{
									status = "Successfully Completed!";
	
								} 
								else 
								{

									status = ChatColor.GRAY + "Incomplete! - type '/challenge task " + j + "'";
								}
								plyer.sendMessage("Challenge Task " + j + ": " + status);
								aCheck = false;
							}
						}
			            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
	                	plyer.sendMessage("==================================");
					} else {
						plyer.sendMessage("Sorry, unrecognised second parameter - " + this.userParams);
					}
					break;
				case 2:
						try 
						{
							int v = Integer.valueOf(args[1]);
							
							if(args[0].equalsIgnoreCase("task"))
							{
								if(v > 0 && v <= this.plugin.getConfig().getInt( "challenge.challenges.tasks" ))
								{
										displayChallenge(v);
							            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
					                	plyer.sendMessage("==================================");
										break;
										
								}
								else
								{
									plyer.sendMessage("Sorry, unrecognised task id - '/challenge task <1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + ">'");
								} 
								
							}else {
									plyer.sendMessage("Sorry, unrecognised second or third parameter - " + this.userParams);
							}
						}
						catch (NumberFormatException e)
						{
							plyer.sendMessage("Sorry, unrecognised second or third parameter - " + this.userParams);
						}
					break;
					
				default:
					
					break;
				}
			}
		} 
		else
		{
        	plyer.sendMessage("==================================");
			plyer.sendMessage("You have already been promoted!");
        	plyer.sendMessage("==================================");
            plyer.sendMessage("Visit: " + this.plugin.getConfig ().getString ( "challenge.web.interface" ) );
        	plyer.sendMessage("==================================");
		}
	}

	/**
	 * 
	 * @param challengeId
	 */
	private void displayChallenge(int challengeId)
	{
		challengeId = challengeId-1;
		if(challengeId >= 0 && challengeId <= (this.plugin.getConfig ().getInt ( "challenge.challenges.tasks" ) - 1))
		{
			ChallengeRecord challengeRecord = this.plugin.getChallengeData().get(challengeId);
        	plyer.sendMessage("==================================");
			plyer.sendMessage("Challenge Task: " + challengeRecord.getChallenge());
        	plyer.sendMessage("==================================");
			
			// check if player has completed this challenge
			// if they have.. display message!
			try
			{
				PreparedStatement statusStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_id FROM player_achievements WHERE player_achievements_ign = '" + this.player.getPlayer_ign() + "' AND player_achievements_challenge = '" + (challengeId + 1)+ "'");
				ResultSet statusResult = statusStatement.executeQuery( );
				if (statusResult.last ()  != false)
				{
					plyer.sendMessage("This challenge task has been successfully completed!");
				} 
				else
				{
					plyer.sendMessage("Slot 1: " + challengeRecord.getSlot0().getQty() +
			    			" x  " + challengeRecord.getSlot0().getItem());
			    	plyer.sendMessage("Slot 2: " + challengeRecord.getSlot1().getQty() +
			    			" x  " + challengeRecord.getSlot1().getItem());
			    	plyer.sendMessage("Slot 3: " + challengeRecord.getSlot2().getQty() +
			    			" x  " + challengeRecord.getSlot2().getItem());
			    	plyer.sendMessage("Slot 4: " + challengeRecord.getSlot3().getQty() +
			    			" x  " + challengeRecord.getSlot3().getItem());
			    	plyer.sendMessage("Slot 5: " + challengeRecord.getSlot4().getQty() +
			    			" x  " + challengeRecord.getSlot4().getItem());
			    	plyer.sendMessage("Slot 6: " + challengeRecord.getSlot5().getQty() +
			    			" x  " + challengeRecord.getSlot5().getItem());
			    	plyer.sendMessage("Slot 7: " + challengeRecord.getSlot6().getQty() +
			    			" x  " + challengeRecord.getSlot6().getItem());
			    	plyer.sendMessage("Slot 8: " + challengeRecord.getSlot7().getQty() +
			    			" x  " + challengeRecord.getSlot7().getItem());
			    	plyer.sendMessage("Slot 9: " + challengeRecord.getSlot8().getQty() +
			    			" x  " + challengeRecord.getSlot8().getItem());
				}

				this.plugin.closeDatabase ();
			}
			catch ( SQLException e )
			{
				e.printStackTrace();
			}
		} else {
        	plyer.sendMessage("==================================");
			plyer.sendMessage("Challenge Task");
        	plyer.sendMessage("==================================");
			plyer.sendMessage("Unrecognised challenge task id requested!");
			plyer.sendMessage("There are " + this.plugin.getConfig ().getInt ( "challenge.challenges.tasks" ) + " challenge tasks.");
		}
	}

	/**
	 * 
	 * @return
	 */
	private boolean checkPromotion()
	{
	    if(
	    	this.plugin.getConfig().getBoolean("challenge.promote.group.enable") == false &&
	    	this.plugin.getConfig().getBoolean("challenge.promote.hero.channel.enable") == false &&
	    	this.plugin.getConfig().getBoolean("challenge.promote.teleport.enable") == false
	    )
	    {
	    	plyer.sendMessage("promotions are currently suspended!");
	    	plyer.sendMessage("please try again later.");
	    } 
	    else 
	    {
	    	// CHECK IF CHALLENGES COMPLETED
	    	int completed = 0;
	    	try
			{
	    		PreparedStatement countStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_challenge FROM player_achievements WHERE player_achievements_ign = '" + this.player.getPlayer_ign() + "' ORDER BY player_achievements_challenge ASC" );
				ResultSet countResult = countStatement.executeQuery( );
				countResult.last();
				completed = countResult.getRow();
				this.plugin.closeDatabase ();
			}
			catch ( SQLException e )
			{
				e.printStackTrace();
			}
	    	
	    	if(completed >= this.plugin.getConfig ().getInt ( "challenge.challenges.tasks" ) &&  this.player.getPlayer_promoted () == false)
	    	{
	    		
	    		 this.broadcastToPlayers ("==================================");
	    		 this.broadcastToPlayers (this.plyerIGN + " has completed all challenge tasks");
	    		 this.broadcastToPlayers ("==================================");
	    		 
	    		 // promotion
				if(this.plugin.getConfig().getBoolean("challenge.promote.group.enable") == true)
				{
					
					this.promoteInWorlds();
				}
				
				// herochat
				if(this.plugin.getConfig().getBoolean("challenge.promote.hero.channel.enable") == true)
				{
					
					// OLD Announce
					//this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " changing channel to " + this.plugin.getConfig().getString("challenge.promote.hero.channel.to"));
					this.plyer.chat("/ch leave " + this.plugin.getConfig().getString("challenge.promote.hero.channel.from"));
					this.plyer.chat("/ch join " + this.plugin.getConfig().getString("challenge.promote.hero.channel.to"));
					this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " changing channel to " + this.plugin.getConfig().getString("challenge.promote.hero.channel.to"));
				}
				
				// teleport
				if(this.plugin.getConfig().getBoolean("challenge.promote.teleport.enable") == true)
				{
					
					// OLD Announce
					// this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + "  teleporting to " + this.plugin.getConfig().getString("challenge.promote.teleport.worldname") + " world!");
					
					String wld = this.plugin.getConfig().getString("challenge.promote.teleport.toworld");
					int x = this.plugin.getConfig().getInt("challenge.promote.teleport.x");
					int y = this.plugin.getConfig().getInt("challenge.promote.teleport.y");
					int z = this.plugin.getConfig().getInt("challenge.promote.teleport.z");
					Location loc = new Location(this.plyer.getServer().getWorld(wld), x, y, z);
				    if(!loc.getChunk().isLoaded())
				    {
				        loc.getChunk().load();
				    }
					this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + "  teleporting to " + this.plugin.getConfig().getString("challenge.promote.teleport.worldname") + " world!");
					this.plyer.teleport(loc);
				}
				
				// add promotion record to db
				try
				{
					Statement promoteStatement = this.plugin.getConn().createStatement ();
					String promote = "UPDATE players SET player_promoted = 'true' WHERE player_ign = '" + this.player.getPlayer_ign () + "'";
		    		int promoteResult = promoteStatement.executeUpdate ( promote );
					if(promoteResult > 0)
					{
						plyer.sendMessage("You have been successfully promoted!");
						return true;
					}
					else
					{
						plyer.sendMessage("Attempted promotion may of failed! Press F2 to take a screenshot and show it to an admin");
					}
					this.plugin.closeDatabase ();
				}
				catch ( SQLException e )
				{
					// add report to config file?
					e.printStackTrace();
				}
	    	} 
	    	else if(completed >= this.plugin.getConfig ().getInt ( "challenge.challenges.tasks" ) &&  this.player.getPlayer_promoted () == true) 
	    	{
	    		plyer.sendMessage("According to our records you have completed");
		    	plyer.sendMessage("all " + this.plugin.getConfig ().getInt ( "challenge.web.interface" ) + " challenges and you have already been promoted.");
				plyer.sendMessage("Press F2 to take a screenshot and show it to an admin");
	    	} 
	    	else 
	    	{
				this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " failed promotion!");
	    	}
		}
		return false;
	}

	private boolean checkChallenge ()
	{
		//++++++++++++++++++++
		// GET ACHIEVEMENTS
		//++++++++++++++++++++
		List <Achievements> aList = new ArrayList <Achievements>();
		try
		{
			PreparedStatement statusStatement = this.plugin.getConn().prepareStatement ("SELECT player_achievements_id, UNIX_TIMESTAMP(player_achievements_date) as achievementDate, player_achievements_challenge FROM player_achievements WHERE player_achievements_ign = '" + this.player.getPlayer_ign() + "' ORDER BY player_achievements_challenge ASC" );
			ResultSet statusResult = statusStatement.executeQuery( );
			if (statusResult.last ()  != false)
			{
				statusResult.beforeFirst ();
				while (statusResult.next())
			    {
					Achievements achievement = new Achievements();
					achievement.setAchievements_id ( statusResult.getInt ( "player_achievements_id" ) );
					achievement.setAchievements_ign ( this.player.getPlayer_ign () );
					achievement.setAchievements_date ( statusResult.getInt ( "achievementDate" ) );
					achievement.setAchievements_challenge ( statusResult.getString ( "player_achievements_challenge" ) );
					aList.add ( achievement );
			    }
			}
		}
		catch ( SQLException e )
		{
			e.printStackTrace();
		}
		
		boolean perm = false;
		
		Date tDate; // today
		tDate = new Date();
		
		Date pDate; // cooldown
		pDate = new Date((long) this.player.getPlayer_cooldown ()*1000);
		
		String challengeID;

		//++++++++++++++++++++
		// CHECK COOLDOWN
		//++++++++++++++++++++
		if(aList.size() > 0)
		{
			
			if(tDate.after(pDate))
			{
				// check allowed
				perm = true;
			} 
			else 
			{
				// no check allowed
				perm = false;
			}
			
		} 
		else 
		{
			perm = true;
		}
		
		//++++++++++++++++++++
		// CHECK CHALLENGE
		//++++++++++++++++++++
		if(perm == true)
		{
			// check inventory matches any challenges?
			for (int i = 0; i < this.plugin.getConfig ().getInt ( "challenge.challenges.tasks" ); i++)
			{
				ChallengeRecord challengeRecord = this.plugin.getChallengeData().get(i);
				challengeID = Integer.toString(i+1);
				
				//++++++++++++++++++++
				// CHECK ID's
				//++++++++++++++++++++
				if(
						( this.invItem[0] == challengeRecord.getSlot0().getId() ) && 
						( this.invItem[1] == challengeRecord.getSlot1().getId() ) && 
						( this.invItem[2] == challengeRecord.getSlot2().getId() ) && 
						( this.invItem[3] == challengeRecord.getSlot3().getId() ) && 
						( this.invItem[4] == challengeRecord.getSlot4().getId() ) && 
						( this.invItem[5] == challengeRecord.getSlot5().getId() ) && 
						( this.invItem[6] == challengeRecord.getSlot6().getId() ) && 
						( this.invItem[7] == challengeRecord.getSlot7().getId() ) && 
						( this.invItem[8] == challengeRecord.getSlot8().getId()) )
				{
					//++++++++++++++++++++
					// CHECK SUB ID's
					//++++++++++++++++++++
					for (int j = 0; j < 9; j++)
					{
						if(
							PlayerCommands.hasDataValue(this.invItem[j]) && this.invDmg[j] != challengeRecord.getSlot(j).getDmg()	
						)
						{
							// an item sub id did not match... 
							// since all main items id's matched in previous if statement
							// it's unlikely the inventory will match another challenge.. 
							// so bail out now!
							
							this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " failed!");
							
							// OLD Announce
							//this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " failed!");
					    	plyer.sendMessage("==================================");
							plyer.sendMessage("Challenge Task Check Failed");
					    	plyer.sendMessage("==================================");
					    	plyer.sendMessage("Tip: Do you have the correct items?");
					    	plyer.sendMessage("Tip: Are the item in the correct slots?");
					    	plyer.sendMessage("Tip: Do you have the correct quantities of each item?");
					    	plyer.sendMessage("Tip: Are all unused hotbar slots empty?");
							return false;
						}
					}

					//++++++++++++++++++++
					// CHECK QUANTITIES
					//++++++++++++++++++++
					if(
							(this.invQty[0] == challengeRecord.getSlot0().getQty()) && 
							(this.invQty[1] == challengeRecord.getSlot1().getQty()) && 
							(this.invQty[2] == challengeRecord.getSlot2().getQty()) && 
							(this.invQty[3] == challengeRecord.getSlot3().getQty()) && 
							(this.invQty[4] == challengeRecord.getSlot4().getQty()) && 
							(this.invQty[5] == challengeRecord.getSlot5().getQty()) && 
							(this.invQty[6] == challengeRecord.getSlot6().getQty()) && 
							(this.invQty[7] == challengeRecord.getSlot7().getQty()) && 
							(this.invQty[8] == challengeRecord.getSlot8().getQty()))
					{
						//++++++++++++++++++++
						// CHECK AGAINST PAST ACHIEVEMENTS
						//++++++++++++++++++++
						for(int k = 0; k < aList.size (); k++)
						{
							if ( Pattern
									.compile (
											Pattern.quote ( aList.get ( k ).getAchievements_challenge () ),
											Pattern.CASE_INSENSITIVE )
									.matcher ( challengeID )
									.find () )
							{
								this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " has already completed challenge!");
								
								// OLD Announce
								// this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " has already completed challenge!");
						    	plyer.sendMessage("==================================");
								plyer.sendMessage("Already Completed This Challenge Task");
						    	plyer.sendMessage("==================================");
					            plyer.sendMessage("If you have completed all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + " challenge tasks");
					            plyer.sendMessage("type '/challenge promote'.");
			                	plyer.sendMessage("Or move on to complete the next challenge task.");
				            	plyer.sendMessage("Type '/challenge task <1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + ">");
				            	plyer.sendMessage("for information on each challenge");
								return true;
							}
						}
						
						//++++++++++++++++++++
						// REMOVE ITEMS FROM INVENTORY
						//++++++++++++++++++++
			        	// remove items form inventory
			        	Inventory inv = plyer.getInventory();
			        	if(challengeRecord.getSlot0().getId() != 0)
			        	{
			        		inv.getItem(0).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot0().getId(), (short) challengeRecord.getSlot0().getDmg(), (int) challengeRecord.getSlot0().getQty()));
			        	}
			        	if(challengeRecord.getSlot1().getId() != 0)
			        	{
				    		inv.getItem(1).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot1().getId(), (short) challengeRecord.getSlot1().getDmg(), (int) challengeRecord.getSlot1().getQty()));
			        	}
			        	if(challengeRecord.getSlot2().getId() != 0)
			        	{
				    		inv.getItem(2).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot2().getId(), (short) challengeRecord.getSlot2().getDmg(), (int) challengeRecord.getSlot2().getQty()));
			        	}
			        	if(challengeRecord.getSlot3().getId() != 0)
			        	{
				    		inv.getItem(3).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot3().getId(), (short) challengeRecord.getSlot3().getDmg(), (int) challengeRecord.getSlot3().getQty()));
			        	}
			        	if(challengeRecord.getSlot4().getId() != 0)
			        	{
				    		inv.getItem(4).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot4().getId(), (short) challengeRecord.getSlot4().getDmg(), (int) challengeRecord.getSlot4().getQty()));
			        	}
			        	if(challengeRecord.getSlot5().getId() != 0)
			        	{
				    		inv.getItem(5).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot5().getId(), (short) challengeRecord.getSlot5().getDmg(), (int) challengeRecord.getSlot5().getQty()));
			        	}
			        	if(challengeRecord.getSlot6().getId() != 0)
			        	{
				    		inv.getItem(6).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot6().getId(), (short) challengeRecord.getSlot6().getDmg(), (int) challengeRecord.getSlot6().getQty()));
			        	}
			        	if(challengeRecord.getSlot7().getId() != 0)
			        	{
				    		inv.getItem(7).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot7().getId(), (short) challengeRecord.getSlot7().getDmg(), (int) challengeRecord.getSlot7().getQty()));
			        	}
			        	if(challengeRecord.getSlot8().getId() != 0)
			        	{
				    		inv.getItem(8).setAmount(PlayerCommands.removeItem((Inventory) inv, (int) challengeRecord.getSlot8().getId(), (short) challengeRecord.getSlot8().getDmg(), (int) challengeRecord.getSlot8().getQty()));
			        	}
    		    		//++++++++++++++++++++
						// GENERATE COOLDOWN
						//++++++++++++++++++++
			    		int min = this.plugin.getConfig().getInt("challenge.challenges." + i + ".delay.min");
			    		int max = this.plugin.getConfig().getInt("challenge.challenges." + i + ".delay.max");
			    		int delay = (min + (int)(Math.random() * ((max - min) + 1))) * 60;
						
			    		// get current time as long (unixtime)
			    		long nowInt = System.currentTimeMillis()/1000;
			    		
			    		// update achievements records (db and local)
			    		try
			    		{
				    		Statement newAchievementStatement = this.plugin.getConn().createStatement ();
				    		String newAchievement = "INSERT INTO player_achievements " +
				    														"(player_achievements_ign, player_achievements_date, player_achievements_challenge) " +
				    														"VALUES " +
				    														"('" + this.player.getPlayer_ign () + "', FROM_UNIXTIME(" + nowInt + "),  '" + challengeID  + "')";
				    		int newAchievementResult = newAchievementStatement.executeUpdate ( newAchievement );
							
				    		if(newAchievementResult > 0)
							{
								try
								{
									Statement newCooldownStatement = this.plugin.getConn().createStatement ();
									String newCooldown = "UPDATE players SET player_cooldown = FROM_UNIXTIME(" + (nowInt+delay) + ") WHERE player_ign = '" + this.player.getPlayer_ign () + "'";
						    		int newCooldownResult = newCooldownStatement.executeUpdate ( newCooldown );
									if(newCooldownResult > 0)
									{
										// update local cooldown
										this.player.setPlayer_cooldown ( (int) nowInt );
										
										// report
										this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " completed: " + challengeRecord.getChallenge());
										
										// OLD Announce
										// this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " completed: " + challengeRecord.getChallenge());

										plyer.sendMessage("==================================");
							    		plyer.sendMessage("Congratulations");
										plyer.sendMessage("==================================");
										plyer.sendMessage(challengeRecord.getAchieved());
							            plyer.sendMessage("If you have completed all " + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + " challenge tasks");
							            plyer.sendMessage("type '/challenge promote'.");
					                	plyer.sendMessage("Or move on to complete the next challenge task.");
						            	plyer.sendMessage("Type '/challenge task <1-" + this.plugin.getConfig().getInt( "challenge.challenges.tasks") + ">");
						            	plyer.sendMessage("for information on each challenge task");
									}
								}
								catch ( SQLException e )
								{
									e.printStackTrace();
								}
							} else {
								plyer.sendMessage("Unable to add achievement to database! press F2 to screenshot error and notify an admin.");
							}
			    		}
						catch ( SQLException e )
						{
							e.printStackTrace();
						}
					} else {
						this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " failed!");
						
						// OLD Announce
						//this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " failed!");
				    	plyer.sendMessage("==================================");
						plyer.sendMessage("Challenge Task Check Failed");
				    	plyer.sendMessage("==================================");
				    	plyer.sendMessage("Tip: Do you have the correct items?");
				    	plyer.sendMessage("Tip: Are the item in the correct slots?");
				    	plyer.sendMessage("Tip: Do you have the correct quantities of each item?");
				    	plyer.sendMessage("Tip: Are all unused hotbar slots empty?");
						return false;
					}
					return true;
				}
			}
		} else {
			
			long cdTimeInSeconds = pDate.getTime(); 
			long curTimeInSeconds = tDate.getTime ();
			long cdInMinutes = ((cdTimeInSeconds-curTimeInSeconds) / (60 * 1000));
			if(cdInMinutes <= 0)
			{
				cdInMinutes = 0;
			}

			this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " denied!");
			
			// OLD Announce
			// this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " denied!");
        	plyer.sendMessage("==================================");
			plyer.sendMessage("Cooldown Period Enabled");
        	plyer.sendMessage("==================================");
			plyer.sendMessage("Please try again after:" + " " + cdInMinutes + "  minutes.");
			return false;
			
		}
		this.broadcastToPlayers("[CHALLENGE] " +  this.plyerIGN + " failed!");
		
		// OLD Announce
		// this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " failed!");
    	plyer.sendMessage("==================================");
		plyer.sendMessage("Challenge Task Check Failed");
    	plyer.sendMessage("==================================");
    	plyer.sendMessage("Tip: Do you have the correct items?");
    	plyer.sendMessage("Tip: Are the item in the correct slots?");
    	plyer.sendMessage("Tip: Do you have the correct quantities of each item?");
    	plyer.sendMessage("Tip: Are all unused hotbar slots empty?");
		this.plugin.closeDatabase ();
		return false;
	}
	
	/**
	 * 
	 * @param inventory
	 * @param id
	 * @param meta
	 * @param quantity
	 * @return
	 */
    public static int removeItem(Inventory inventory, int id, short meta, int quantity) {
        int rest = quantity;
        for( int i = 0 ; i < inventory.getSize() ; i++ ){
            ItemStack stack = inventory.getItem(i);
            if( stack == null || stack.getTypeId() != id )
                continue;
            if( PlayerCommands.hasDataValue(id) && stack.getDurability() != meta ){
                continue;
            }
            if( rest >= stack.getAmount() ){
                rest -= stack.getAmount();
                inventory.clear(i);
            } else if( rest>0 ){
                    stack.setAmount(stack.getAmount()-rest);
                    rest = 0;
            } else {
                break;
            }
        }
        return quantity-rest;
    }
    
	/**
     * 
     * @param id
     * @return
     */
    public static boolean hasDataValue (int id){
        if( id == 6 )
            return true;
        if( id == 17 )
            return true;
        if( id == 18 )
            return true;
        if( id == 24 )
            return true;
        if( id == 31 )
            return true;
        if( id == 35 )
            return true;
        if( id == 43 )
            return true;
        if( id == 44 )
            return true;
        if( id == 97 )
            return true;
        if( id == 98 )
            return true;
        if( id == 263 )
            return true;
        if( id == 351 )
            return true;
        if( id == 373 )
            return true;
        if( id == 383 )
            return true;
        return false;
    }
    
    /**
     * 
     * @param date
     * @return
     */
    public Date removeTime(Date date) {    
        Calendar cal = Calendar.getInstance();  
        cal.setTime(date);  
        cal.set(Calendar.HOUR_OF_DAY, 0);  
        cal.set(Calendar.MINUTE, 0);  
        cal.set(Calendar.SECOND, 0);  
        cal.set(Calendar.MILLISECOND, 0);  
        return cal.getTime(); 
    }
    
    private void broadcastToPlayers (String msg)
    {
    	// send message to all opted in staff
    	this.broadcastToActiveStaff ( msg );
    	
    	// get all online players
    	Player[] ps = this.plugin.getServer ().getOnlinePlayers ();
    	
    	// loop players and send Message
    	for (Player p:ps)
    	{
    		if(p.hasPermission ( "challenge.mute" ) == false)
    		{
    			p.sendMessage (msg);
    		}
    	}
    }
    
    private void broadcastToActiveStaff(String msg)
    {
    	// get all online players
    	List < Player > ps = this.plugin.getActiveStaff();
    	
    	// loop players and send Message
    	if(ps.size () > 0) // fix for null list
		{
	    	for (Player p:ps)
	    	{
	    		if(p.isOnline ())
	    		{
	    			p.sendMessage (msg);
	    		} else {
	    			ps.remove(p);
	    			
	    		}
	    	}
		}
    }
    
    private void promoteInWorlds()
    {
		Server svr = this.plugin.getServer();
		ConsoleCommandSender ccs = svr.getConsoleSender();
		List< String > names = this.plugin.getConfig ().getStringList ( "challenge.promote.group.names" );
		List< String > worlds = this.plugin.getConfig ().getStringList ( "challenge.promote.group.worlds" );
		List< String > cmds = this.plugin.getConfig ().getStringList ( "challenge.promote.group.commands" );
		String rCMD = "";
		
    	if(worlds.size() > 0)
    	{
			for ( int i = 0; i < worlds.size (); i++ )
			{
				if(this.plugin.getConfig().getBoolean("challenge.promote.permissions.permissionsEx") == true)
				{
					try 
					{
						switch (cmds.get ( i ))
						{
							case "add":
								rCMD = "add";
							break;
							case "remove":
								rCMD = "remove";
							break;
						default:
							rCMD = "set";
						}
						
						String wld = worlds.get ( i );
						if(wld == "null")
						{
							wld = "";
						}
						svr.dispatchCommand(ccs, "pex user "+ this.plyer.getName() +" group " + rCMD + " " + names.get(i) + " " + wld);
					}
					catch (Exception e) {
						this.plugin.log.info("[REPORT] Please check config 'challenge.promote.group' to ensure you have equal amount of names/worlds/commands.");
				    	plyer.sendMessage("[Challenge] Failed to promote, notify an Admin to check the console log for an error report!");
					}
				}
				else
				{
					try 
					{
						switch (cmds.get ( i ))
						{
							case "manudel":
								rCMD = "manudel";
								svr.dispatchCommand(ccs, "manselect " + worlds.get ( i ));
								svr.dispatchCommand(ccs, rCMD + " " + this.plyer.getName());
								svr.dispatchCommand(ccs, "manclear");
							break;
						default:
							rCMD = "manuadd";
							svr.dispatchCommand(ccs, "manselect " + worlds.get ( i ));
							svr.dispatchCommand(ccs, rCMD + " " + this.plyer.getName() + " " + names.get(i));
							svr.dispatchCommand(ccs, "manclear");
						}
					}
					catch (Exception e) {
						this.plugin.log.info("[REPORT] Please check config 'challenge.promote.group' to ensure you have equal amount of names/worlds/commands.");
				    	plyer.sendMessage("[Challenge] Failed to promote, notify an Admin to check the console log for an error report!");
					}
				}
			}
			this.plugin.getServer().broadcastMessage("[CHALLENGE] " +  this.plyerIGN + " has been promoted!");
    	} else {
    		this.plugin.getServer().broadcastMessage("[CHALLENGE] Unable to promote " +  this.plyerIGN + ", no worlds found to promote player to in the config, please notify an admin!");
    	}
		
    }
}